import io.kotest.assertions.asClue
import io.kotest.matchers.file.shouldContainNFiles
import io.kotest.matchers.paths.shouldContainFiles
import io.kotest.matchers.paths.shouldExist
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import java.io.File
import java.nio.file.Paths

class CreateSerumSmartFoldersTest {
    val testSerumDirPath = "./src/test/testData/Serum/"
    val testPresetsDirFoo = "$testSerumDirPath/Presets"
    private val testSerumDirArgs = "--serum-dir $testSerumDirPath"

    @BeforeEach
    internal fun setUp() {
        val testSerumDir = File(testPresetsDirFoo)
        //before recursive delete of the test dir do some sanity checks
        if (!testSerumDir.isDirectory || testSerumDir.parentFile.parentFile.name != "testData") {
            fail("Can't delete this dir, since it doesn't exist as a dir or appear to be the test Serum dir: " + testSerumDir.canonicalFile)
        }
        getSmartDirs()
                .forEach {
                    it.deleteRecursively() shouldBe true
                }
    }

    private fun getSmartDirs() =
            File(testPresetsDirFoo).walk().filter { file -> file.isDirectory && file.name.contains("^\\d\\d__".toRegex()) }
                    .toList()

    private val PRESET_CATEGORY_BASS = "Category Bass"
    private val PRESET_RATING_4 = "Rating 4"
    private val PRESET_RATING_5 = "Rating 5"
    private val PRESET_USER = "Just Sub, 2 octave pitch bend"

    val allPresetNames = listOf(PRESET_USER, PRESET_CATEGORY_BASS, PRESET_RATING_4, PRESET_RATING_5)
    val allRating4and5PresetNames = listOf(PRESET_RATING_4, PRESET_RATING_5)

    @Test
    internal fun `default args`() {
        // given
        val args = testSerumDirArgs.toArgs()

        // when
        main(args)

        // then
        assertSmartDir("01__User", allPresetNames)
        assertSmartDir("02__Ratings4and5", allRating4and5PresetNames)
        assertSmartDir("03__Recently-Added", allPresetNames)
        assertSmartDir("05__Bass", listOf(PRESET_CATEGORY_BASS))
        assertSmartDirCount(4)
    }

    @Test
    internal fun `--skip-recent`() {
        // given
        val args = "$testSerumDirArgs --skip-recent".toArgs()

        // when
        main(args)

        // then
        assertSmartDir("01__User", listOf(PRESET_USER))
        assertSmartDir("02__Ratings4and5", allRating4and5PresetNames)
        assertSmartDir("05__Bass", listOf(PRESET_CATEGORY_BASS))
        assertSmartDirCount(3)
    }

    @Test
    internal fun `--skip-user`() {
        // given
        val args = "$testSerumDirArgs --skip-user".toArgs()

        // when
        main(args)

        // then
        assertSmartDir("02__Ratings4and5", allRating4and5PresetNames)
        assertSmartDir("03__Recently-Added", allPresetNames)
        assertSmartDir("05__Bass", listOf(PRESET_CATEGORY_BASS))
        assertSmartDirCount(3)
    }

    @Test
    internal fun `--skip-categories`() {
        // given
        val args = "$testSerumDirArgs --skip-categories".toArgs()

        // when
        main(args)

        // then
        assertSmartDir("01__User", listOf(PRESET_USER))
        assertSmartDir("02__Ratings4and5", allRating4and5PresetNames)
        assertSmartDir("03__Recently-Added", allPresetNames)
        assertSmartDirCount(3)
    }

    @Test
    internal fun `--skip-ratings`() {
        // given
        val args = "$testSerumDirArgs --skip-ratings".toArgs()

        // when
        main(args)

        // then
        assertSmartDir("01__User", listOf(PRESET_USER))
        assertSmartDir("03__Recently-Added", allPresetNames)
        assertSmartDir("05__Bass", listOf(PRESET_CATEGORY_BASS))
        assertSmartDirCount(3)
    }

    @Test
    internal fun `default args (run twice to exercise cleanup`() {
        // given
        val args = testSerumDirArgs.toArgs()

        // when
        main(args)
        main(args)
        //TODO: add a real assert. For now confirm "Deleting stale sym link: $path" appears in log, maybe with a spy on logger

        // then
        assertSmartDir("01__User", allPresetNames)
        assertSmartDir("02__Ratings4and5", allRating4and5PresetNames)
        assertSmartDir("03__Recently-Added", allPresetNames)
        assertSmartDir("05__Bass", listOf(PRESET_CATEGORY_BASS))
        assertSmartDirCount(4)
    }

    private fun assertSmartDirCount(expectedSmartDirCount: Int) {
        File(testPresetsDirFoo).also { dir ->
            "Found smart dirs ${getSmartDirs()}".asClue {
                dir.shouldContainNFiles(expectedSmartDirCount + 1) //also consider real User dir
            }
        }
    }

    private fun assertSmartDir(expectedFolderName: String, expectedPresetNames: List<String>) {
        Paths.get(testPresetsDirFoo, expectedFolderName).also { path ->
            "Found smart dirs ${getSmartDirs()}".asClue {
                path.shouldExist()
            }
            path.shouldContainFiles(*(expectedPresetNames.map { "$it.fxp" }.toTypedArray()))
        }
    }
}


private fun String.toArgs() = this.split(" ").toTypedArray()
