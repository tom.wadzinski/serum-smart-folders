#!/usr/bin/env kotlin

/*
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


//The following @file annotations are used when converting this file to a main.kts script

@file:Repository("https://jcenter.bintray.com")
@file:DependsOn("org.jetbrains.exposed:exposed-core:0.28.1")
@file:DependsOn("org.jetbrains.exposed:exposed-dao:0.28.1")
@file:DependsOn("org.jetbrains.exposed:exposed-jdbc:0.28.1")
@file:DependsOn("org.jetbrains.exposed:exposed-java-time:0.28.1")
@file:DependsOn("org.xerial:sqlite-jdbc:3.23.1")
@file:DependsOn("org.slf4j:slf4j-nop:1.7.25")
@file:DependsOn("com.offbytwo:docopt:0.6.0.20150202")

import org.docopt.Docopt
import org.docopt.DocoptExitException
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.`java-time`.datetime
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

import java.io.IOException
import java.nio.file.Files
import kotlin.script.experimental.dependencies.DependsOn
import kotlin.script.experimental.dependencies.Repository
import kotlin.system.exitProcess

val smartFolderPrefix = "%02d__"

/**
 * Note: This script hasn't been tested on windows.
 *
 * See this link for resolution to possible windows permission issue: https://stackoverflow.com/questions/19230535/create-windows-symbolic-link-with-java-equivalent-to-mklink
 *
 * To edit this file, you can use any text editor. For better code insight (error checking, command completion), open this file in JetBrains IntelliJ.
 *
 * This app can be run as a normal main() or can be converted to a standalong kts script with ./gradle build, which creates the kts script in the ./build directory
 */
fun main(args: Array<String>) {
    createSerumSmartFolders(args)
}

fun createSerumSmartFolders(args: Array<String>) {
    val usage = """
        Create Serum "Smart" preset folders, that filter and group Serum presets into numerically prefixed folders
        using symbolic links to the actual presets.
        This is useful especially for selecting presets with MIDI program changes messages from a MIDI controller.
        
        Example (using kotlin directly): kotlin createSerumSmartFolders.kts -- --categories Bass,SFX,Pad
        
        Usage: createSerumSmartFolders.kts [options] 
        
        Options:
         --categories LIST   Comma separated list of categories. Smart folders for each will be created. [default: Lead,Bass]
         --skip-categories   Don't include categories smart folders
         --skip-user         Don't include "User" smart folder (which is normally the top smart folder and is a copy of User, but in a fixed location)
         --skip-ratings      Don't include "Ratings4and5" smart folder (which includes all presets rated 4 or 5 ordered by date added)
         --skip-recent       Don't include "Recently-Added" smart folder (which includes the 50 most recently added presets)
         --serum-dir FILE    [default: /Library/Audio/Presets/Xfer Records/Serum Presets]
        """.trimIndent()

    //parse arguments
    val argsMap = try {
        Docopt(usage).withExit(false).parse(args.toList())
    } catch (e: DocoptExitException) {
        System.err.println("Bad command line arguments: ${args.joinToString(separator = " ")}")
        System.err.println("HELP:\n$usage")

        exitProcess(e.exitCode)
    }

    val serumDir =
        File(argsMap["--serum-dir"] as String).also { if (!it.exists()) throw RuntimeException("Unable to find serum base dir (contains System and Presets dirs): ${it.canonicalPath}") }
    val presetDbDir =
        File("$serumDir/System").also { if (!it.exists()) throw RuntimeException("Unable to find serum system dir (contains Serum presetdb.dat db file: ${it.canonicalPath}") }
    val presetsDir =
        File("$serumDir/Presets").also { if (!it.exists()) throw RuntimeException("Unable to find serum presets dir: ${it.canonicalPath}") }

    val smartFolderDefinitions = buildSmartFolderDefinitions(argsMap)

    removeExistingSmartLinks(presetsDir, smartFolderDefinitions)

    connectToSerumDbAndCreateSmartFolders(smartFolderDefinitions, presetDbDir, presetsDir, serumDir)
}

fun buildSmartFolderDefinitions(argsMap: MutableMap<String, Any>): ArrayList<SmartFolderDefinition> {
    val smartFolderDefinitions = ArrayList<SmartFolderDefinition>()

    if (argsMap["--skip-user"] == false) {
        smartFolderDefinitions.add(SmartFolderDefinition("User", SerumPresets.select { SerumPresets.presetRelativePath eq "User" }))
    } else {
        smartFolderDefinitions.add(spacerDefinition)
    }

    if (argsMap["--skip-ratings"] == false) {
        smartFolderDefinitions.add(SmartFolderDefinition("Ratings4and5",
            SerumPresets
                .select { SerumPresets.rating greaterEq 4 }
                .orderBy(SerumPresets.dbEntryCreated to SortOrder.DESC)))
    } else {
        smartFolderDefinitions.add(spacerDefinition)
    }

    if (argsMap["--skip-recent"] == false) {
        smartFolderDefinitions.add(SmartFolderDefinition("Recently-Added",
            SerumPresets
                .selectAll()
                .orderBy(SerumPresets.dbEntryCreated to SortOrder.DESC)
                .limit(50)))
    } else {
        smartFolderDefinitions.add(spacerDefinition)
    }

    val categories = (argsMap["--categories"] as String).split(",").map { it.trim() }
    if (argsMap["--skip-categories"] == false) {
        smartFolderDefinitions.addAll(categories.map { category -> SmartFolderDefinition(category, SerumPresets.select { SerumPresets.category eq category }) })
    }

    //Add some spacers to allow for more categories, while still maintaining folder names (NOTE: doesn't really matter if folder names change though, maybe as long as order stays the same...)
    repeat(10) {
        smartFolderDefinitions.add(spacerDefinition)
    }

    //NOTE: you could add additional smart folders here
    //See query docs for details on constructing queries: https://github.com/JetBrains/Exposed/wiki/DSL#where-expression
    // For example:
    //smartFolderDefinitions.add(SmartFolderDefinition("Duda", SerumPresets.select { SerumPresets.author like "%Duda%" }))

    return smartFolderDefinitions
}

fun connectToSerumDbAndCreateSmartFolders(
    smartFolderDefinitions: ArrayList<SmartFolderDefinition>,
    presetDbDir: File,
    presetsDir: File,
    serumPresetsDir: File
) {
    Database.connect("jdbc:sqlite:${presetDbDir}/presetdb.dat", driver = "org.sqlite.JDBC")
    transaction {
        addLogger(StdOutSqlLogger)

        smartFolderDefinitions.forEachIndexed { dirPrefixIndex, smartFolderDefinition ->
            if (!smartFolderDefinition.spacer) {
                val linksDir = getLinksDir(presetsDir, smartFolderDefinition, dirPrefixIndex + 1)
                val presets =
                    SerumPreset.wrapRows(smartFolderDefinition.query) //runs query and converts results to SerumPreset object
                if (!presets.empty()) {
                    //Create links dir if needed
                    linksDir.mkdir()
                }
                presets.forEach {
                    createSymbolicLink(
                        serumPresetsDir,
                        presetsDir.resolve("${it.presetRelativePath}/${it.presetDisplayName}.fxp"),
                        linksDir
                    )
                }
            }
        }
    }
}

@Throws(IOException::class)
fun createSymbolicLink(serumPresetsDir: File, target: File, linksDir: File) {
    val link = File(linksDir, target.name)

    if (Files.isSymbolicLink(link.toPath())) {
        link.delete()
        println("Duplicate preset name found, preferring: " + target.canonicalPath)
    }
    val linkDescription = link.canonicalPath.replace(serumPresetsDir.canonicalPath, "<serum dir>")
    val targetDescription = target.canonicalPath.replace(serumPresetsDir.canonicalPath, "<serum dir>")
    println("Creating symbolic link: $linkDescription -> $targetDescription")

    Files.createSymbolicLink(link.toPath(), target.toPath())
}

fun removeExistingSmartLinks(presetsDir: File, smartFolderDefinitions: MutableCollection<SmartFolderDefinition>) {
    smartFolderDefinitions.forEachIndexed { dirPrefixIndex, fooBar ->
        val linksDir = getLinksDir(presetsDir, fooBar, dirPrefixIndex + 1)

        if (linksDir.exists()) {
            Files.walk(linksDir.toPath())
                .filter { path ->
                    Files.isSymbolicLink(path) && path.toString().endsWith(".fxp", ignoreCase = true)
                }
                .forEach { path ->
                    println("Deleting stale sym link: $path")
                    Files.delete(path)
                }
        }
    }
}

fun getLinksDir(presetsDir: File, smartFolderDefinition: SmartFolderDefinition, dirPrefixIndex: Int): File {
    val linksFolderName = String.format(
        "$smartFolderPrefix${smartFolderDefinition.folderName}",
        dirPrefixIndex
    ) // produces e.g. 01__Ratings
    return presetsDir.resolve(linksFolderName)
}

object SerumPresets : IntIdTable("SerumPresetTable", "EntryID") {
    val dbEntryCreated = datetime("dbEntryCreated")
    val dateCreated = datetime("DateCreated")
    val presetDisplayName = varchar("PresetDisplayName", 9999)
    val presetRelativePath = varchar("PresetRelativePath", 9999)
    val author = varchar("Author", 9999)
    val description = varchar("Description", 9999)
    val rating = integer("Rating")
    val category = varchar("Category", 9999)
}

@Suppress("MemberVisibilityCanBePrivate")
class SerumPreset(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<SerumPreset>(SerumPresets)

    var dbEntryCreated by SerumPresets.dbEntryCreated
    var dateCreated by SerumPresets.dateCreated
    var presetDisplayName by SerumPresets.presetDisplayName
    var presetRelativePath by SerumPresets.presetRelativePath
    var author by SerumPresets.author
    var description by SerumPresets.description
    var rating by SerumPresets.rating
    var category by SerumPresets.category

    override fun toString(): String {
        return "SerumPreset(dbEntryCreated=$dbEntryCreated, dateCreated=$dateCreated, presetDisplayName='$presetDisplayName', rating=$rating, category='$category')"
    }
}

val spacerDefinition = SmartFolderDefinition(
    spacer = true,
    folderName = "",
    query = SerumPresets.select { SerumPresets.presetDisplayName eq "this is ignored...." }
)

data class SmartFolderDefinition(val folderName: String, val query: Query, val spacer: Boolean = false)
