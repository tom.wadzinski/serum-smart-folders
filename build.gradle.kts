import java.io.StringReader

plugins {
    kotlin("jvm") version "1.4.20"
}

group = "com.lazyengineer"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlin:kotlin-scripting-dependencies:1.4.20")
    implementation("org.jetbrains.kotlin:kotlin-script-runtime:1.4.20")

    implementation("org.jetbrains.exposed:exposed-core:0.28.1")
    implementation("org.jetbrains.exposed:exposed-dao:0.28.1")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.28.1")
    implementation("org.jetbrains.exposed:exposed-java-time:0.28.1")
    implementation("org.xerial:sqlite-jdbc:3.23.1")
    implementation("org.slf4j:slf4j-nop:1.7.25")
    implementation("com.offbytwo:docopt:0.6.0.20150202")

    testImplementation(platform("org.junit:junit-bom:5.7.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("io.kotest:kotest-assertions-core:4.3.1")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

// config JVM target to 1.8 for kotlin compilation tasks
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
    kotlinOptions.jvmTarget = "1.8"
}

// Convert the script in .kt form to main.kts form so it can be run standalone (just adds in a call to main at the bottom of the file and created the renamed file)
val buildCreateSerumSmartFoldersScript = tasks.register<Copy>("buildCreateSerumSmartFoldersScript") {
    group = "build"

    val scriptNameNoExtension = "createSerumSmartFolders"
    val ktFile = "$scriptNameNoExtension.kt"
    from("${project.projectDir}/src/main/kotlin/$ktFile") {
        val launchScriptLine = """
                                  
                                  // Calls the script's main function.
                                  main(args)
                                  """.trimIndent()
        filter<org.apache.tools.ant.filters.ConcatFilter>("appendReader" to StringReader(launchScriptLine))
    }

    into("${project.buildDir}/")
    rename(ktFile, "${scriptNameNoExtension}.main.kts") // convert to kts main script file

    // 'up to date' checking (only run if source was changed)
    inputs.files(project.fileTree(project.projectDir).include("**/$ktFile"))
}

val assembleTask: Task by project.tasks.named("assemble")
assembleTask.dependsOn(buildCreateSerumSmartFoldersScript)
