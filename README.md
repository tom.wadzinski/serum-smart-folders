## serum-smart-folders

serum-smart-folders is a Kotlin script to create Serum "Smart" preset folders that filter and group your Serum presets into numerically prefixed folders
using symbolic links to the actual presets. These will then appear at the top of the Serum preset chooser, making them predictable when selecting via MIDI Program Change messages.
TODO add additional notes - defaults, etc...

Latest script release: http://tom.wadzinski.gitlab.io/serum-smart-folders/createSerumSmartFolders.main.kts

# Installation and running:
* This is a script written with the Kotlin language, so first install Kotlin: https://kotlinlang.org/docs/tutorials/command-line.html (on macOS I use homebrew)
* <a href="http://tom.wadzinski.gitlab.io/serum-smart-folders/createSerumSmartFolders.main.kts" download="createSerumSmartFolders.main.kts">Download the latest script file(Right-click on this link and select 'Save As...'</a>  and save it anywhere on your local file system with the name createSerumSmartFolders.main.kts

* Open up a terminal/command prompt and cd to the directory where you saved the script
* Run `kotlin createSerumSmartFolders.main.kts`
* by default this will create the following smart folders: a user folder(copy of User but always at the top, a "Ratings4and5" folder, a "recently added" folder, and two category folders (Bass and Lead)

To see more command line options run `kotlin createSerumSmartFolders.main.kts -- --help` 

* Rerun the script (with the same command line options) whenever you want to refresh your smart folders. Note that if you change the command line options, you will want to manually delete the smart folders to clean up the clutter.


#### Modifying the script
The script itself is plain text, so you can fairly easily modify the script to add additional smart folders. Modifying the script might require a bit of software development skills, however, I added some notes in the script to guide you through creating new filters to create additional smart folders.

